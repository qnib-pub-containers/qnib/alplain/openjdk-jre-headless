ARG FROM_IMG_REGISTRY=registry.gitlab.com
ARG FROM_IMG_REPO=qnib-pub-containers/qnib/alplain
ARG FROM_IMG_NAME=init
ARG FROM_IMG_TAG="2020-04-13-5"

FROM ${DOCKER_REGISTRY}/${FROM_IMG_REPO}/${FROM_IMG_NAME}:${FROM_IMG_TAG}${DOCKER_IMG_HASH}

RUN apk --no-cache add openjdk17-jre-headless
